WorkerDescription
=================

.. autoclass:: mcai_worker_sdk.WorkerDescription
  :members:
  :undoc-members:
  :special-members: __init__
