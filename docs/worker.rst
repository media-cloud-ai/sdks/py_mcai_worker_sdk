Worker
======

  .. autoclass:: mcai_worker_sdk.Worker

    .. automethod:: setup
    
    .. automethod:: init_process
      
    .. automethod:: process_frames

    .. automethod:: process
    
    .. automethod:: ending_process

    .. automethod:: start
      