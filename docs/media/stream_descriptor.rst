.. _stream_descriptors:

Stream descriptors
==================

Collection of classes to define the streams to process. It supports either audio and video stream (with FFmpeg filters that can be applied) or simple data streams.

.. autoclass:: mcai_worker_sdk.AudioStreamDescriptor
  :members:

.. autoclass:: mcai_worker_sdk.VideoStreamDescriptor
  :members:

.. autoclass:: mcai_worker_sdk.DataStreamDescriptor
  :members:

.. autoclass:: mcai_worker_sdk.StreamDescriptor
  :members:
